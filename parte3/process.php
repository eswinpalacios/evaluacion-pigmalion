<?php

 	include ("exerciseChangeString1.php");
 	include ("exerciseClearPar1.php");
 	include ("exerciseCompleteRange1.php");

 	header("Content-type:application/json");

	$type = "";
	$input = "";
	$out = "";
	$state = 0;
	$msj = "output complete!";

	if(isset($_POST['type']))
		$type = $_POST['type'];

	if(isset($_POST['input']))
		$input = $_POST['input'];

	if($type == "" or $input == "")
	{
		$state = 1;
		$msj = "error. parametro type o input sin valor!";
	}

	switch ($type) {
		case 'ChangeString':
			$out = getChangeString($input); 
			break;
		case 'ClearPar':
			$out = getClearPar($input); 
			break;
		case 'CompleteRange':
			$out = getCompleteRange($input); 
			break;		
	}

	$response = [];
	$response["state"] = $state;
	$response["msj"] = $msj;
	$response["type"] = $type;
	$response["out"] = $out;

	echo json_encode($response);

	function getChangeString($input)
	{
		$inputs = explode("\n", $input);
		$output = "";
		
		foreach ($inputs as $key => $in)
			$output .= ChangeString::build($in)."\n";

		return $output;
	}

	function getClearPar($input)
	{
		$inputs = explode("\n", $input);
		$output = "";

		foreach ($inputs as $key => $in)
			$output .= ClearPar::build($in)."\n";

		return $output;
	}

	function getCompleteRange($input)
	{
		$inputs = explode("\n", $input);
		$output = "";

		foreach ($inputs as $key => $in){
			$numbers = explode(",", $in);
			$output .= getArrayToString(CompleteRange::build($numbers))."\n";
		}			

		return $output;
	}

	function getArrayToString($numbers)
	{
		$string = "";
		
		for ($i=0; $i <count($numbers) ; $i++)
			$string .= $numbers[$i].", ";

		return $string;
	}

?>
