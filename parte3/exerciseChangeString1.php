<?php 

#Forma 1. Más rápida

class ChangeString
{

	public static function build($oldString)
	{
		$start = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$finish = "bcdefghijklmnopqrstuvwxyzaBCDEFGHIJKLMNOPQRSTUVWXYZA";

		return strtr($oldString, $start, $finish);
	}
		
}

//var_dump(ChangeString::build("123 abc*3"));
//var_dump(ChangeString::build("**Casa 52"));
//var_dump(ChangeString::build("**Casa 52Z"));

?>
