<?php

#Forma 2

class ClearPar
{

	public static function build($string)
	{
		$stringNew = "";
		$listOpen = [];
		$listClose = [];

		for ($i=0; $i < strlen($string); $i++) { 
			
			if($string[$i] == "(")
				array_push($listOpen, $i);
			else if($string[$i] == ")")
			{
				if($i==0 or count($listOpen) == 0)
					array_push($listClose, $i);
				else if(count($listOpen) > 0)
					array_pop($listOpen);
			}

		}

		for ($i=0; $i < strlen($string); $i++) { 
			
			if(!(in_array($i, $listOpen) or in_array($i, $listClose)))
				$stringNew .= $string[$i];

		}

		return $stringNew;
	}

}

var_dump(ClearPar::build("()())()"));
var_dump(ClearPar::build("()(()"));
var_dump(ClearPar::build(")("));
var_dump(ClearPar::build("((()"));

?>
