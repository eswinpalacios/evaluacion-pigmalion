<?php 

#Forma 2

class ChangeString
{

	public function build($oldString)
	{
		$newString = "";

		for ($i=0; $i < strlen($oldString); $i++)
			$newString .= $this->changeCharacter($oldString[$i]);		

		return $newString;
	}

	private function changeCharacter($character)
	{
		if(ord($character) == 90)
			return "A";
		else if(ord($character) == 122)
			return "Z";
		else if( (65 <= ord($character) and ord($character) <= 90) or (97 <= ord($character) and ord($character) <= 122) )
			return chr(ord($character)+1);
		
		return $character;
	}
		
}

$changeString = new ChangeString();

var_dump($changeString->build("123 abc*3"));
var_dump($changeString->build("**Casa 52"));
var_dump($changeString->build("**Casa 52Z"));

?>
