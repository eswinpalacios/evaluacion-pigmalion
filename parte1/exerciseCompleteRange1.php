<?php

class CompleteRange
{

	public static function build($elements)
	{
		$first = $elements[0];
		$last = $elements[ count($elements)-1 ];

		$elementsNew = [];

		for ($i=$first; $i <= $last; $i++)
			array_push($elementsNew, $i);

		return $elementsNew;
	}

}

$completeRange = new CompleteRange();

var_dump(CompleteRange::build([1, 2, 4, 5] ));
var_dump(CompleteRange::build([2, 4, 9] ));
var_dump(CompleteRange::build([55, 58, 60]));

?>
