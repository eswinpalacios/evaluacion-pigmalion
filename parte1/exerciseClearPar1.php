<?php

#Forma 1. Más rápida

class ClearPar
{

	public static function build($string)
	{
		$stringTemp = "";
		$stringNew = "";

		for ($i=0; $i < strlen($string); $i++) { 
			

			if($string[$i] == ")")
			{
				if(strlen($stringTemp) > 0)
				{
					$stringNew .= $string[$i-1].$string[$i];
					$stringTemp = substr($stringTemp, 0, -1);
				}
			}
			else 
				$stringTemp .= $string[$i];

		}

		return $stringNew;
	}

}

var_dump(ClearPar::build("()())()"));
var_dump(ClearPar::build("()(()"));
var_dump(ClearPar::build(")("));
var_dump(ClearPar::build("((()"));

?>
