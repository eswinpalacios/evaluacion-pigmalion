<?php
// Routes

$app->get('/', function ($request, $response, $args) {        
    
    return $this->renderer->render($response, 'index.phtml', $args);

});

$app->post('/getEmployees', function ($request, $response, $args) {    

	$email = $request->getParam('email', "");
	$email = trim($email);
	$employeesFilter = [];   
    
    $newResponse = $response->withHeader('Content-type', 'application/json');
    $str = file_get_contents("employees.json");    
    $employees = json_decode($str, true);

    if($email == ""){
		
        foreach ($employees as $key => $employee) {
            unset($employee["skills"]);
            array_push($employeesFilter, $employee);
        }
        
    }
	else
	{
		
        foreach ($employees as $key => $employee) {

			if(strpos($employee["email"], $email) !== false ){                
                unset($employee["skills"]);
                array_push($employeesFilter, $employee);
            }				

    	}

	}

    $newResponse = $response->withJson($employeesFilter);
	
    return $newResponse;
});

$app->post('/getEmployee', function ($request, $response, $args) {    

    $id = $request->getParam('id', "");
    $id = trim($id);
    $employeesFilter = [];   
    
    $newResponse = $response->withHeader('Content-type', 'application/json');
    $str = file_get_contents("employees.json");    
    $employees = json_decode($str, true);

    if($id == "")
        $newResponse = $response->withJson([]);     
    else
    {
        foreach ($employees as $key => $employee) {

            if(strpos($employee["id"], $id) !== false ){
                $employeesFilter = $employee;
                break;
            }

        }

        $newResponse = $response->withJson($employeesFilter);
    }
    
    return $newResponse;
});

$app->post('/service/filterByRemuneration', function ($request, $response, $args) {  

	$rangeMin = $request->getParam('rangeMin', 0);
	$rangeMax = $request->getParam('rangeMax', 0);
	$employeesFilter = [];

	$str = file_get_contents("employees.json");
    $employees = json_decode($str, true);    

    foreach ($employees as $key => $employee) {    	
    	$salary = $employee["salary"];	
    	
    	$salary = str_replace("$", "", $salary);
    	$salary = str_replace(",", "", $salary);

		if($rangeMin <= $salary and $salary <= $rangeMax)
		{
			$salary = floatval($salary);			
			array_push($employeesFilter, $employee);
		}

    }

    $xml = '<?xml version="1.0" encoding="UTF-8"?>';
    $xml .= "<employees>";
    foreach ($employeesFilter as $index => $employee) {
    	$xml .= "<employee$index>";
    	foreach ($employee as $key => $value) {
    		$xml .= "<$key>";

    		if(is_array($value))
    		{

    			foreach ($value as $indexArray => $valueElement) {
					$xml .= "<skill>";
					$xml .= $valueElement["skill"];
					$xml .= "</skill>";
    			}
    		}
    		else
    			$xml .= $value;

    		$xml .= "</$key>";
    	}
    	$xml .= "</employee$index>";
    }

    $xml .= "</employees>";
    
    $body = $response->getBody();	
	$body->write($xml);

    $newResponse = $response->withHeader('Content-type', 'text/xml');
    return $newResponse;
});
