$( document ).ready(function() { 

	$( "#btn-buscar" ).click(function() {
  		getEmployees();
	});

	getEmployees();

});

function loadEvent()
{
	$( ".a-detail" ).click(function(e) {
  		e.preventDefault();
  		getEmployee(this);
	});
}

function getEmployee(obj)
{
	var id = $( obj ).data( "id");	

	$.post( "getEmployee", { id: id })
  	.done(function( data ) {    	
		showModalEmployee(data);		
  	});
}

function showModalEmployee(data)
{
	var skills = "";

	$("#modal-Employee").modal("show");
	$("#name").val( data.name );
	$("#email").val( data.email );
	$("#phone").val( data.phone );
	$("#address").val( data.address );
	$("#position").val( data.position );
	$("#salary").val( data.salary );	

	for (var i = 0; i < data.skills.length; i++) {
		skills += ' <span class="label label-success">' + data.skills[i].skill + '</span>  ';		
	}

	$("#skills").html(skills);
}

function getEmployees()
{
	var email = $("#txt-email").val();

	$.post( "getEmployees", { email: email })
  	.done(function( data ) {  		    	
		$("#tb-employees > tbody").html("");
    	showEmployees(data);
    	loadEvent();
  	});
}

function showEmployees(employees)
{
	console.log(employees);
	for (var i = 0; i < employees.length; i++) {
		showEmployee(employees[i], i+1);
	}
}

function showEmployee(employee, number)
{
	var row = "";

	row = "<tr>";
	row += "<td>" + number + "</td>";
	row += "<td>" + employee.name + "</td>";
	row += "<td>" + employee.email + "</td>";
	row += "<td>" + employee.position + "</td>";
	row += "<td>" + employee.salary + "</td>";
	row += "<td> <a href='#' data-id='" + employee.id + "' class='a-detail' >ver</a> </td>";
	row += "</tr>";

	$('#tb-employees').append(row);
}
